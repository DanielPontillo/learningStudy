var learningStudy = [
{
trialnum: -5,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pomme",
option1: "pommette",
option2: "petite pomme",
option3: "pommeau",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2052}]
}
,

{
trialnum: -4,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "ballon",
option1: "ballonelle",
option2: "ballonette",
option3: "petit ballon",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2053}]
}
,

{
trialnum: -3,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chat",
option1: "chats",
option2: "chates",
option3: "chat\u00e9es",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2054}]
}
,

{
trialnum: -2,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bouteille",
option1: "bouteilles",
option2: "bouteillez",
option3: "bouteillees",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2055}]
}
,

{
trialnum: -1,
block: 0,
miniblock: 0,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "plural",
rootWord: "chien",
option1: "chieni",
option2: "chienne",
option3: "chiens",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2056}]
}
,

{
trialnum: 0,
block: 0,
miniblock: 0,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "size",
rootWord: "chapeau",
option1: "petit chapeau",
option2: "chapette",
option3: "chapelle",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2057}]
}
,

{
trialnum: 1,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boupi",
option2: "boup\u00e9",
option3: "boub\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2002}]
}
,

{
trialnum: 2,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dujonp",
option2: "dijonp",
option3: "dojonp",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2005}]
}
,

{
trialnum: 3,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "voun",
option1: "duvoun",
option2: "dovoun",
option3: "divoun",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2021}]
}
,

{
trialnum: 4,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koumi",
option2: "koun\u00e9",
option3: "koum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2020}]
}
,

{
trialnum: 5,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "duchinp",
option2: "dichinp",
option3: "dochinp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 6,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beun\u00e9",
option2: "beumi",
option3: "beum\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 7,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dujinch",
option2: "dojinch",
option3: "dijinch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2023}]
}
,

{
trialnum: 8,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "divouv",
option2: "duvouv",
option3: "dovouv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2015}]
}
,

{
trialnum: 9,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "geub",
option1: "geupi",
option2: "geup\u00e9",
option3: "geub\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2040}]
}
,

{
trialnum: 10,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "domanp",
option2: "dimanp",
option3: "dumanp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 11,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vani",
option2: "van\u00e9",
option3: "vam\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2026}]
}
,

{
trialnum: 12,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "disuf",
option2: "dusuf",
option3: "dosuf",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 13,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dogouf",
option2: "digouf",
option3: "dugouf",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 14,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "dominb",
option3: "diminb",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 15,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "douni",
option2: "doun\u00e9",
option3: "doum\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2004}]
}
,

{
trialnum: 16,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gan\u00e9",
option2: "gam\u00e9",
option3: "gani",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2036}]
}
,

{
trialnum: 17,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dubip",
option2: "dobip",
option3: "dibip",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2025}]
}
,

{
trialnum: 18,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanf\u00e9",
option2: "chanfi",
option3: "chanv\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2014}]
}
,

{
trialnum: 19,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "dominb",
option3: "diminb",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 20,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dukun",
option2: "dikun",
option3: "dokun",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2013}]
}
,

{
trialnum: 21,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panpi",
option2: "panb\u00e9",
option3: "panp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2016}]
}
,

{
trialnum: 22,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gupi",
option2: "gup\u00e9",
option3: "guz\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 23,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonb",
option1: "gonpi",
option2: "gonp\u00e9",
option3: "gonb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 24,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fim",
option1: "difim",
option2: "dufim",
option3: "dofim",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2031}]
}
,

{
trialnum: 25,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fip",
option1: "dufip",
option2: "dofip",
option3: "difip",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2035}]
}
,

{
trialnum: 26,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "dimonch",
option2: "dumonch",
option3: "domonch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2017}]
}
,

{
trialnum: 27,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finch\u00e9",
option2: "finchi",
option3: "finj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 28,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dijonp",
option2: "dujonp",
option3: "dojonp",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2005}]
}
,

{
trialnum: 29,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "duchinp",
option2: "dichinp",
option3: "dochinp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 30,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "niv\u00e9",
option2: "nivi",
option3: "nif\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2030}]
}
,

{
trialnum: 31,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonch\u00e9",
option2: "gonchi",
option3: "gonj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 32,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "domanp",
option2: "dimanp",
option3: "dumanp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 33,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "digouf",
option2: "dugouf",
option3: "dogouf",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 34,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kan\u00e9",
option2: "kani",
option3: "kam\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2032}]
}
,

{
trialnum: 35,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guv\u00e9",
option2: "guf\u00e9",
option3: "gufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 36,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "dib",
option1: "dudib",
option2: "dodib",
option3: "didib",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 37,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tinch",
option1: "tinj\u00e9",
option2: "tinchi",
option3: "tinch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2024}]
}
,

{
trialnum: 38,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dusuf",
option2: "disuf",
option3: "dosuf",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 39,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "dufeum",
option2: "difeum",
option3: "dofeum",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 40,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finchi",
option2: "finch\u00e9",
option3: "finj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 41,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanf",
option1: "duchanf",
option2: "dochanf",
option3: "dichanf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 42,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kapi",
option2: "kap\u00e9",
option3: "kab\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 43,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "domonch",
option2: "dimonch",
option3: "dumonch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2017}]
}
,

{
trialnum: 44,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dibip",
option2: "dobip",
option3: "dubip",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2025}]
}
,

{
trialnum: 45,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fip",
option1: "dofip",
option2: "difip",
option3: "dufip",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2035}]
}
,

{
trialnum: 46,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panb\u00e9",
option2: "panp\u00e9",
option3: "panpi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 47,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boup\u00e9",
option2: "boub\u00e9",
option3: "boupi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 48,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koumi",
option2: "koum\u00e9",
option3: "koun\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2020}]
}
,

{
trialnum: 49,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "niv\u00e9",
option2: "nivi",
option3: "nif\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2030}]
}
,

{
trialnum: 50,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonchi",
option2: "gonch\u00e9",
option3: "gonj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 51,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pib\u00e9",
option2: "pip\u00e9",
option3: "pipi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2034}]
}
,

{
trialnum: 52,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vam\u00e9",
option2: "vani",
option3: "van\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2026}]
}
,

{
trialnum: 53,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dokoub",
option2: "dukoub",
option3: "dikoub",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 54,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "dosiv",
option2: "dusiv",
option3: "disiv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2033}]
}
,

{
trialnum: 55,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doum\u00e9",
option2: "douni",
option3: "doun\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2004}]
}
,

{
trialnum: 56,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souv\u00e9",
option2: "souf\u00e9",
option3: "soufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 57,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dikuch",
option2: "dukuch",
option3: "dokuch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2011}]
}
,

{
trialnum: 58,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gani",
option2: "gam\u00e9",
option3: "gan\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2036}]
}
,

{
trialnum: 59,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "guz\u00e9",
option2: "gup\u00e9",
option3: "gupi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 60,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souv\u00e9",
option2: "souf\u00e9",
option3: "soufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 61,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dojinch",
option2: "dijinch",
option3: "dujinch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2023}]
}
,

{
trialnum: 62,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dikun",
option2: "dokun",
option3: "dukun",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 63,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beun\u00e9",
option2: "beumi",
option3: "beum\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 64,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dikuch",
option2: "dokuch",
option3: "dukuch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 65,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minbi",
option2: "minp\u00e9",
option3: "minb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 66,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "dogam",
option2: "dugam",
option3: "digam",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2036}]
}
,

{
trialnum: 67,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "dugeub",
option2: "dogeub",
option3: "digeub",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2040}]
}
,

{
trialnum: 68,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kap",
option1: "dukap",
option2: "dokap",
option3: "dikap",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 69,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "digup",
option2: "dogup",
option3: "dugup",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 70,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dotinch",
option2: "ditinch",
option3: "dutinch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2024}]
}
,

{
trialnum: 71,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kun",
option1: "kum\u00e9",
option2: "kuni",
option3: "kun\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2013}]
}
,

{
trialnum: 72,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "monch",
option1: "monch\u00e9",
option2: "monji",
option3: "monj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2017}]
}
,

{
trialnum: 73,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "siv",
option1: "sivi",
option2: "siv\u00e9",
option3: "siv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2033}]
}
,

{
trialnum: 74,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fip\u00e9",
option2: "fib\u00e9",
option3: "fibi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2035}]
}
,

{
trialnum: 75,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "dogonb",
option2: "digonb",
option3: "dugonb",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2008}]
}
,

{
trialnum: 76,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "voum\u00e9",
option2: "voun\u00e9",
option3: "vouni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2021}]
}
,

{
trialnum: 77,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manbi",
option2: "manb\u00e9",
option3: "manp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2009}]
}
,

{
trialnum: 78,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fimi",
option2: "fim\u00e9",
option3: "fin\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2031}]
}
,

{
trialnum: 79,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "donif",
option2: "dinif",
option3: "dunif",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2030}]
}
,

{
trialnum: 80,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "dosouv",
option2: "dusouv",
option3: "disouv",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2012}]
}
,

{
trialnum: 81,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "didoum",
option2: "dudoum",
option3: "dodoum",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2004}]
}
,

{
trialnum: 82,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "digonb",
option2: "dugonb",
option3: "dogonb",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 83,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanv\u00e9",
option2: "chanfi",
option3: "chanf\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2001}]
}
,

{
trialnum: 84,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "voum\u00e9",
option2: "vouni",
option3: "voun\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2021}]
}
,

{
trialnum: 85,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dupanp",
option2: "dopanp",
option3: "dipanp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2016}]
}
,

{
trialnum: 86,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manb\u00e9",
option2: "manp\u00e9",
option3: "manbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 87,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonbi",
option2: "jonb\u00e9",
option3: "jonp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 88,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "difinch",
option2: "dufinch",
option3: "dofinch",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2038}]
}
,

{
trialnum: 89,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "dusouv",
option2: "disouv",
option3: "dosouv",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2012}]
}
,

{
trialnum: 90,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dibi",
option3: "dib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 91,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "monch",
option1: "monch\u00e9",
option2: "monji",
option3: "monj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2017}]
}
,

{
trialnum: 92,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "duchanv",
option2: "dochanv",
option3: "dichanv",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2014}]
}
,

{
trialnum: 93,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fib\u00e9",
option2: "fip\u00e9",
option3: "fibi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2035}]
}
,

{
trialnum: 94,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "digam",
option2: "dugam",
option3: "dogam",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2036}]
}
,

{
trialnum: 95,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dobeun",
option2: "dibeun",
option3: "dubeun",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2028}]
}
,

{
trialnum: 96,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "sufi",
option2: "suf\u00e9",
option3: "suv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2007}]
}
,

{
trialnum: 97,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "suf\u00e9",
option2: "sufi",
option3: "suv\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2007}]
}
,

{
trialnum: 98,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koun",
option1: "dikoun",
option2: "dukoun",
option3: "dokoun",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2020}]
}
,

{
trialnum: 99,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "boup",
option1: "duboup",
option2: "diboup",
option3: "doboup",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2002}]
}
,

{
trialnum: 100,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bibi",
option2: "bip\u00e9",
option3: "bib\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2025}]
}
,

{
trialnum: 101,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "dogonch",
option2: "digonch",
option3: "dugonch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 102,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feumi",
option2: "feum\u00e9",
option3: "feun\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2037}]
}
,

{
trialnum: 103,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "duguv",
option2: "doguv",
option3: "diguv",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2006}]
}
,

{
trialnum: 104,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dichanv",
option2: "dochanv",
option3: "duchanv",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 105,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jinch",
option1: "jinji",
option2: "jinch\u00e9",
option3: "jinj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2023}]
}
,

{
trialnum: 106,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koubi",
option2: "koub\u00e9",
option3: "koup\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 107,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "dudoum",
option2: "dodoum",
option3: "didoum",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2004}]
}
,

{
trialnum: 108,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "gouf\u00e9",
option2: "gouv\u00e9",
option3: "goufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 109,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dufinch",
option2: "dofinch",
option3: "difinch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 110,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dupib",
option2: "dopib",
option3: "dipib",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 111,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dibi",
option3: "dib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 112,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouv\u00e9",
option2: "vouvi",
option3: "vouf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2015}]
}
,

{
trialnum: 113,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fim\u00e9",
option2: "fin\u00e9",
option3: "fimi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 114,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dupib",
option2: "dopib",
option3: "dipib",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 115,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "goufi",
option2: "gouv\u00e9",
option3: "gouf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 116,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dukam",
option2: "dokam",
option3: "dikam",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2032}]
}
,

{
trialnum: 117,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "divam",
option2: "duvam",
option3: "dovam",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2026}]
}
,

{
trialnum: 118,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouvi",
option2: "vouv\u00e9",
option3: "vouf\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2015}]
}
,

{
trialnum: 119,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chinp",
option1: "chinp\u00e9",
option2: "chinbi",
option3: "chinb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2039}]
}
,

{
trialnum: 120,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuj\u00e9",
option2: "kuji",
option3: "kuch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 121,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dibeun",
option2: "dobeun",
option3: "dubeun",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2028}]
}
,

{
trialnum: 122,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "donif",
option2: "dunif",
option3: "dinif",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2030}]
}
,

{
trialnum: 123,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feun\u00e9",
option2: "feumi",
option3: "feum\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 124,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "duvam",
option2: "dovam",
option3: "divam",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2026}]
}
,

{
trialnum: 125,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dutinch",
option2: "dotinch",
option3: "ditinch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2024}]
}
,

{
trialnum: 126,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koubi",
option2: "koub\u00e9",
option3: "koup\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 127,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dikam",
option2: "dukam",
option3: "dokam",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2032}]
}
,

{
trialnum: 128,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanfi",
option2: "chanf\u00e9",
option3: "chanv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 129,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "disuf",
option2: "dosuf",
option3: "dusuf",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2007}]
}
,

{
trialnum: 130,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dugouf",
option2: "dogouf",
option3: "digouf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2003}]
}
,

{
trialnum: 131,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonb",
option1: "gonp\u00e9",
option2: "gonpi",
option3: "gonb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 132,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "geub",
option1: "geub\u00e9",
option2: "geupi",
option3: "geup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2040}]
}
,

{
trialnum: 133,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kap\u00e9",
option2: "kapi",
option3: "kab\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 134,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanf",
option1: "dichanf",
option2: "dochanf",
option3: "duchanf",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2001}]
}
,

{
trialnum: 135,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vam\u00e9",
option2: "vani",
option3: "van\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2026}]
}
,

{
trialnum: 136,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonch\u00e9",
option2: "gonj\u00e9",
option3: "gonchi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 137,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panp\u00e9",
option2: "panb\u00e9",
option3: "panpi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 138,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "nif\u00e9",
option2: "nivi",
option3: "niv\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2030}]
}
,

{
trialnum: 139,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pip\u00e9",
option2: "pipi",
option3: "pib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 140,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dikun",
option2: "dokun",
option3: "dukun",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 141,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beumi",
option2: "beun\u00e9",
option3: "beum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 142,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souv\u00e9",
option2: "souf\u00e9",
option3: "soufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 143,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "domonch",
option2: "dimonch",
option3: "dumonch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2017}]
}
,

{
trialnum: 144,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "disiv",
option2: "dosiv",
option3: "dusiv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2033}]
}
,

{
trialnum: 145,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanv\u00e9",
option2: "chanfi",
option3: "chanf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2014}]
}
,

{
trialnum: 146,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dojonp",
option2: "dujonp",
option3: "dijonp",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 147,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dokuch",
option2: "dikuch",
option3: "dukuch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 148,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koum\u00e9",
option2: "koun\u00e9",
option3: "koumi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2020}]
}
,

{
trialnum: 149,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dukun",
option2: "dikun",
option3: "dokun",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2013}]
}
,

{
trialnum: 150,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fip",
option1: "dofip",
option2: "dufip",
option3: "difip",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2035}]
}
,

{
trialnum: 151,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dujinch",
option2: "dijinch",
option3: "dojinch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2023}]
}
,

{
trialnum: 152,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "dominb",
option2: "diminb",
option3: "duminb",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2029}]
}
,

{
trialnum: 153,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gup\u00e9",
option2: "guz\u00e9",
option3: "gupi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 154,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "dosiv",
option2: "disiv",
option3: "dusiv",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2033}]
}
,

{
trialnum: 155,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dosuf",
option2: "dusuf",
option3: "disuf",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2007}]
}
,

{
trialnum: 156,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dugouf",
option2: "dogouf",
option3: "digouf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2003}]
}
,

{
trialnum: 157,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kapi",
option2: "kab\u00e9",
option3: "kap\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2022}]
}
,

{
trialnum: 158,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boupi",
option2: "boub\u00e9",
option3: "boup\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2002}]
}
,

{
trialnum: 159,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "duvouv",
option2: "dovouv",
option3: "divouv",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2015}]
}
,

{
trialnum: 160,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guv\u00e9",
option2: "guf\u00e9",
option3: "gufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 161,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "dumonch",
option2: "dimonch",
option3: "domonch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2017}]
}
,

{
trialnum: 162,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gam\u00e9",
option2: "gan\u00e9",
option3: "gani",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2036}]
}
,

{
trialnum: 163,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dokoub",
option2: "dukoub",
option3: "dikoub",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 164,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finchi",
option2: "finch\u00e9",
option3: "finj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 165,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dobip",
option2: "dibip",
option3: "dubip",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2025}]
}
,

{
trialnum: 166,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beumi",
option2: "beun\u00e9",
option3: "beum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 167,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boub\u00e9",
option2: "boupi",
option3: "boup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2002}]
}
,

{
trialnum: 168,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tinch",
option1: "tinch\u00e9",
option2: "tinj\u00e9",
option3: "tinchi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2024}]
}
,

{
trialnum: 169,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "domanp",
option2: "dimanp",
option3: "dumanp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 170,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souf\u00e9",
option2: "souv\u00e9",
option3: "soufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 171,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panp\u00e9",
option2: "panpi",
option3: "panb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2016}]
}
,

{
trialnum: 172,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dukuch",
option2: "dokuch",
option3: "dikuch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 173,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "dichinp",
option2: "dochinp",
option3: "duchinp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2039}]
}
,

{
trialnum: 174,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "niv\u00e9",
option2: "nif\u00e9",
option3: "nivi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2030}]
}
,

{
trialnum: 175,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gupi",
option2: "gup\u00e9",
option3: "guz\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 176,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "dominb",
option3: "diminb",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 177,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doun\u00e9",
option2: "douni",
option3: "doum\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2004}]
}
,

{
trialnum: 178,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "divouv",
option2: "duvouv",
option3: "dovouv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2015}]
}
,

{
trialnum: 179,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kan\u00e9",
option2: "kam\u00e9",
option3: "kani",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2032}]
}
,

{
trialnum: 180,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fim",
option1: "dufim",
option2: "dofim",
option3: "difim",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2031}]
}
,

{
trialnum: 181,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "dib",
option1: "dodib",
option2: "didib",
option3: "dudib",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2027}]
}
,

{
trialnum: 182,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pib\u00e9",
option2: "pipi",
option3: "pip\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2034}]
}
,

{
trialnum: 183,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "difeum",
option2: "dufeum",
option3: "dofeum",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 184,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guf\u00e9",
option2: "guv\u00e9",
option3: "gufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 185,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "voun",
option1: "divoun",
option2: "dovoun",
option3: "duvoun",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2021}]
}
,

{
trialnum: 186,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "dichinp",
option2: "dochinp",
option3: "duchinp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2039}]
}
,

{
trialnum: 187,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dokoub",
option2: "dukoub",
option3: "dikoub",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 188,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "dumanp",
option2: "dimanp",
option3: "domanp",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2009}]
}
,

{
trialnum: 189,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kan\u00e9",
option2: "kani",
option3: "kam\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2032}]
}
,

{
trialnum: 190,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonj\u00e9",
option2: "gonch\u00e9",
option3: "gonchi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 191,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "difeum",
option2: "dufeum",
option3: "dofeum",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 192,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koumi",
option2: "koum\u00e9",
option3: "koun\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2020}]
}
,

{
trialnum: 193,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fim\u00e9",
option2: "fimi",
option3: "fin\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2031}]
}
,

{
trialnum: 194,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feumi",
option2: "feum\u00e9",
option3: "feun\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2037}]
}
,

{
trialnum: 195,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kap",
option1: "dukap",
option2: "dokap",
option3: "dikap",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 196,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bibi",
option2: "bib\u00e9",
option3: "bip\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2025}]
}
,

{
trialnum: 197,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minb\u00e9",
option2: "minp\u00e9",
option3: "minbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2029}]
}
,

{
trialnum: 198,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "siv",
option1: "sivi",
option2: "siv\u00e9",
option3: "siv\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2033}]
}
,

{
trialnum: 199,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "dogam",
option2: "dugam",
option3: "digam",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2036}]
}
,

{
trialnum: 200,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kun",
option1: "kum\u00e9",
option2: "kun\u00e9",
option3: "kuni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 201,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dukam",
option2: "dikam",
option3: "dokam",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2032}]
}
,

{
trialnum: 202,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manp\u00e9",
option2: "manbi",
option3: "manb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2009}]
}
,

{
trialnum: 203,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koub\u00e9",
option2: "koubi",
option3: "koup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 204,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "sufi",
option2: "suf\u00e9",
option3: "suv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2007}]
}
,

{
trialnum: 205,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "gouv\u00e9",
option2: "gouf\u00e9",
option3: "goufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 206,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "dunif",
option2: "donif",
option3: "dinif",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2030}]
}
,

{
trialnum: 207,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "diguv",
option2: "duguv",
option3: "doguv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2006}]
}
,

{
trialnum: 208,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dipib",
option2: "dupib",
option3: "dopib",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2034}]
}
,

{
trialnum: 209,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "boup",
option1: "diboup",
option2: "doboup",
option3: "duboup",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 210,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "doguv",
option2: "duguv",
option3: "diguv",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2006}]
}
,

{
trialnum: 211,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouf\u00e9",
option2: "vouv\u00e9",
option3: "vouvi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2015}]
}
,

{
trialnum: 212,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "dovam",
option2: "divam",
option3: "duvam",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2026}]
}
,

{
trialnum: 213,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "voun\u00e9",
option2: "voum\u00e9",
option3: "vouni",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2021}]
}
,

{
trialnum: 214,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "dugup",
option2: "digup",
option3: "dogup",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 215,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dipanp",
option2: "dopanp",
option3: "dupanp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 216,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "dugonch",
option2: "dogonch",
option3: "digonch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2010}]
}
,

{
trialnum: 217,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "goufi",
option2: "gouv\u00e9",
option3: "gouf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 218,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "digonb",
option2: "dugonb",
option3: "dogonb",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 219,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dichanv",
option2: "dochanv",
option3: "duchanv",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 220,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dotinch",
option2: "dutinch",
option3: "ditinch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2024}]
}
,

{
trialnum: 221,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonbi",
option2: "jonb\u00e9",
option3: "jonp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 222,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minb\u00e9",
option2: "minp\u00e9",
option3: "minbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2029}]
}
,

{
trialnum: 223,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dubeun",
option2: "dobeun",
option3: "dibeun",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 224,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fin\u00e9",
option2: "fim\u00e9",
option3: "fimi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 225,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dochanv",
option2: "dichanv",
option3: "duchanv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 226,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanv\u00e9",
option2: "chanf\u00e9",
option3: "chanfi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2001}]
}
,

{
trialnum: 227,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonp\u00e9",
option2: "jonb\u00e9",
option3: "jonbi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2005}]
}
,

{
trialnum: 228,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koun",
option1: "dukoun",
option2: "dikoun",
option3: "dokoun",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2020}]
}
,

{
trialnum: 229,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koup\u00e9",
option2: "koubi",
option3: "koub\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2019}]
}
,

{
trialnum: 230,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chinp",
option1: "chinp\u00e9",
option2: "chinbi",
option3: "chinb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2039}]
}
,

{
trialnum: 231,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dib\u00e9",
option3: "dibi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2027}]
}
,

{
trialnum: 232,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "disouv",
option2: "dusouv",
option3: "dosouv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2012}]
}
,

{
trialnum: 233,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "sufi",
option2: "suv\u00e9",
option3: "suf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 234,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "donif",
option2: "dinif",
option3: "dunif",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2030}]
}
,

{
trialnum: 235,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuj\u00e9",
option2: "kuji",
option3: "kuch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 236,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jinch",
option1: "jinch\u00e9",
option2: "jinji",
option3: "jinj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2023}]
}
,

{
trialnum: 237,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouvi",
option2: "vouf\u00e9",
option3: "vouv\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2015}]
}
,

{
trialnum: 238,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "dugeub",
option2: "digeub",
option3: "dogeub",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2040}]
}
,

{
trialnum: 239,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "dogam",
option2: "digam",
option3: "dugam",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2036}]
}
,

{
trialnum: 240,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "dodoum",
option2: "didoum",
option3: "dudoum",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 241,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "dodoum",
option2: "didoum",
option3: "dudoum",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 242,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "monch",
option1: "monji",
option2: "monch\u00e9",
option3: "monj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2017}]
}
,

{
trialnum: 243,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dutinch",
option2: "ditinch",
option3: "dotinch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2024}]
}
,

{
trialnum: 244,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jinch",
option1: "jinch\u00e9",
option2: "jinji",
option3: "jinj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2023}]
}
,

{
trialnum: 245,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dubeun",
option2: "dibeun",
option3: "dobeun",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2028}]
}
,

{
trialnum: 246,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dofinch",
option2: "dufinch",
option3: "difinch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 247,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuj\u00e9",
option2: "kuji",
option3: "kuch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 248,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "dogeub",
option2: "dugeub",
option3: "digeub",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2040}]
}
,

{
trialnum: 249,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dopib",
option2: "dupib",
option3: "dipib",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 250,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fibi",
option2: "fib\u00e9",
option3: "fip\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2035}]
}
,

{
trialnum: 251,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kap",
option1: "dukap",
option2: "dokap",
option3: "dikap",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 252,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dopanp",
option2: "dipanp",
option3: "dupanp",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 253,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fibi",
option2: "fip\u00e9",
option3: "fib\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2035}]
}
,

{
trialnum: 254,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "digonb",
option2: "dugonb",
option3: "dogonb",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 255,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bib\u00e9",
option2: "bip\u00e9",
option3: "bibi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2025}]
}
,

{
trialnum: 256,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "siv",
option1: "siv\u00e9",
option2: "sivi",
option3: "siv\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2033}]
}
,

{
trialnum: 257,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fiv",
option1: "fivi",
option2: "fiv\u00e9",
option3: "fif\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2049}]
}
,

{
trialnum: 258,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bap",
option1: "dobap",
option2: "dibap",
option3: "dubap",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2050}]
}
,

{
trialnum: 259,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gaut",
option1: "dogaut",
option2: "digaut",
option3: "dugaut",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2046}]
}
,

{
trialnum: 260,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "daid",
option1: "didaid",
option2: "dodaid",
option3: "dudaid",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2044}]
}
,

{
trialnum: 261,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaid",
option1: "kaid\u00e9",
option2: "kait\u00e9",
option3: "kaidi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2043}]
}
,

{
trialnum: 262,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jaut",
option1: "dijaut",
option2: "dojaut",
option3: "dujaut",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2042}]
}
,

{
trialnum: 263,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaud",
option1: "kaut\u00e9",
option2: "kaudi",
option3: "kaud\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2045}]
}
,

{
trialnum: 264,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "mait",
option1: "dumait",
option2: "domait",
option3: "dimait",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2048}]
}
,

{
trialnum: 265,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tait",
option1: "tait\u00e9",
option2: "taid\u00e9",
option3: "taidi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2047}]
}
,

{
trialnum: 266,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "baud",
option1: "baut\u00e9",
option2: "baud\u00e9",
option3: "baudi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2041}]
}
,

{
trialnum: 267,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gaut",
option1: "digaut",
option2: "dogaut",
option3: "dugaut",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2046}]
}
,

{
trialnum: 268,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tait",
option1: "taid\u00e9",
option2: "tait\u00e9",
option3: "taidi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2047}]
}
,

{
trialnum: 269,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaud",
option1: "kaudi",
option2: "kaud\u00e9",
option3: "kaut\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2045}]
}
,

{
trialnum: 270,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "baud",
option1: "baut\u00e9",
option2: "baud\u00e9",
option3: "baudi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2041}]
}
,

{
trialnum: 271,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fiv",
option1: "fiv\u00e9",
option2: "fif\u00e9",
option3: "fivi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2049}]
}
,

{
trialnum: 272,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "mait",
option1: "dumait",
option2: "domait",
option3: "dimait",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2048}]
}
,

{
trialnum: 273,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaid",
option1: "kaid\u00e9",
option2: "kait\u00e9",
option3: "kaidi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2043}]
}
,

{
trialnum: 274,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bap",
option1: "dobap",
option2: "dibap",
option3: "dubap",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2050}]
}
,

{
trialnum: 275,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "daid",
option1: "dudaid",
option2: "dodaid",
option3: "didaid",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2044}]
}
,

{
trialnum: 276,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jaut",
option1: "dujaut",
option2: "dijaut",
option3: "dojaut",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2042}]
}
,
];export default learningStudy