var learningStudy = [
{
trialnum: -5,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pomme",
option1: "pommette",
option2: "petite pomme",
option3: "pommeau",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2052}]
}
,

{
trialnum: -4,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "ballon",
option1: "ballonelle",
option2: "ballonette",
option3: "petit ballon",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2053}]
}
,

{
trialnum: -3,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chat",
option1: "chats",
option2: "chates",
option3: "chat\u00e9es",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2054}]
}
,

{
trialnum: -2,
block: 0,
miniblock: 0,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bouteille",
option1: "bouteilles",
option2: "bouteillez",
option3: "bouteillees",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2055}]
}
,

{
trialnum: -1,
block: 0,
miniblock: 0,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "plural",
rootWord: "chien",
option1: "chieni",
option2: "chienne",
option3: "chiens",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2056}]
}
,

{
trialnum: 0,
block: 0,
miniblock: 0,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "size",
rootWord: "chapeau",
option1: "petit chapeau",
option2: "chapette",
option3: "chapelle",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2057}]
}
,

{
trialnum: 1,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "voun",
option1: "duvoun",
option2: "dovoun",
option3: "divoun",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2021}]
}
,

{
trialnum: 2,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beun\u00e9",
option2: "beumi",
option3: "beum\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 3,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dikoub",
option2: "dokoub",
option3: "dukoub",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2019}]
}
,

{
trialnum: 4,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "dochinp",
option2: "dichinp",
option3: "duchinp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2039}]
}
,

{
trialnum: 5,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanfi",
option2: "chanf\u00e9",
option3: "chanv\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2014}]
}
,

{
trialnum: 6,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "dumonch",
option2: "domonch",
option3: "dimonch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2017}]
}
,

{
trialnum: 7,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dusuf",
option2: "disuf",
option3: "dosuf",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 8,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "dib",
option1: "dudib",
option2: "didib",
option3: "dodib",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2027}]
}
,

{
trialnum: 9,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kani",
option2: "kan\u00e9",
option3: "kam\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2032}]
}
,

{
trialnum: 10,
block: 1,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dijinch",
option2: "dojinch",
option3: "dujinch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2023}]
}
,

{
trialnum: 11,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dokuch",
option2: "dikuch",
option3: "dukuch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 12,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pib\u00e9",
option2: "pipi",
option3: "pip\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2034}]
}
,

{
trialnum: 13,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanf",
option1: "dochanf",
option2: "duchanf",
option3: "dichanf",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 14,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dugouf",
option2: "digouf",
option3: "dogouf",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 15,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tinch",
option1: "tinj\u00e9",
option2: "tinchi",
option3: "tinch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2024}]
}
,

{
trialnum: 16,
block: 1,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finch\u00e9",
option2: "finj\u00e9",
option3: "finchi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2038}]
}
,

{
trialnum: 17,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kapi",
option2: "kap\u00e9",
option3: "kab\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 18,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "nivi",
option2: "nif\u00e9",
option3: "niv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2030}]
}
,

{
trialnum: 19,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dobip",
option2: "dibip",
option3: "dubip",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2025}]
}
,

{
trialnum: 20,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "divouv",
option2: "dovouv",
option3: "duvouv",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2015}]
}
,

{
trialnum: 21,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "diminb",
option3: "dominb",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2029}]
}
,

{
trialnum: 22,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finchi",
option2: "finj\u00e9",
option3: "finch\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2038}]
}
,

{
trialnum: 23,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonb",
option1: "gonpi",
option2: "gonp\u00e9",
option3: "gonb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 24,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vam\u00e9",
option2: "van\u00e9",
option3: "vani",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2026}]
}
,

{
trialnum: 25,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koumi",
option2: "koum\u00e9",
option3: "koun\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2020}]
}
,

{
trialnum: 26,
block: 1,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dugouf",
option2: "digouf",
option3: "dogouf",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 27,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panpi",
option2: "panb\u00e9",
option3: "panp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2016}]
}
,

{
trialnum: 28,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dikun",
option2: "dokun",
option3: "dukun",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 29,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guf\u00e9",
option2: "guv\u00e9",
option3: "gufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 30,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dijonp",
option2: "dojonp",
option3: "dujonp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2005}]
}
,

{
trialnum: 31,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boub\u00e9",
option2: "boupi",
option3: "boup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2002}]
}
,

{
trialnum: 32,
block: 1,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dusuf",
option2: "disuf",
option3: "dosuf",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 33,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fim",
option1: "difim",
option2: "dofim",
option3: "dufim",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 34,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souv\u00e9",
option2: "soufi",
option3: "souf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2012}]
}
,

{
trialnum: 35,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gupi",
option2: "gup\u00e9",
option3: "guz\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 36,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "dimanp",
option2: "domanp",
option3: "dumanp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 37,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "dufeum",
option2: "dofeum",
option3: "difeum",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2037}]
}
,

{
trialnum: 38,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "dosiv",
option2: "disiv",
option3: "dusiv",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2033}]
}
,

{
trialnum: 39,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "geub",
option1: "geub\u00e9",
option2: "geupi",
option3: "geup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2040}]
}
,

{
trialnum: 40,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guf\u00e9",
option2: "guv\u00e9",
option3: "gufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 41,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boub\u00e9",
option2: "boup\u00e9",
option3: "boupi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 42,
block: 1,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dukun",
option2: "dokun",
option3: "dikun",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2013}]
}
,

{
trialnum: 43,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonch\u00e9",
option2: "gonchi",
option3: "gonj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 44,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "dichinp",
option2: "duchinp",
option3: "dochinp",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 45,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doum\u00e9",
option2: "douni",
option3: "doun\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2004}]
}
,

{
trialnum: 46,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanv\u00e9",
option2: "chanfi",
option3: "chanf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2014}]
}
,

{
trialnum: 47,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dokoub",
option2: "dukoub",
option3: "dikoub",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 48,
block: 1,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dujinch",
option2: "dijinch",
option3: "dojinch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2023}]
}
,

{
trialnum: 49,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doun\u00e9",
option2: "doum\u00e9",
option3: "douni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 50,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fip",
option1: "dufip",
option2: "difip",
option3: "dofip",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2035}]
}
,

{
trialnum: 51,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gam\u00e9",
option2: "gani",
option3: "gan\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2036}]
}
,

{
trialnum: 52,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panpi",
option2: "panp\u00e9",
option3: "panb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2016}]
}
,

{
trialnum: 53,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tinch",
option1: "tinj\u00e9",
option2: "tinch\u00e9",
option3: "tinchi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2024}]
}
,

{
trialnum: 54,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dokuch",
option2: "dikuch",
option3: "dukuch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 55,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonchi",
option2: "gonj\u00e9",
option3: "gonch\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2010}]
}
,

{
trialnum: 56,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dijonp",
option2: "dujonp",
option3: "dojonp",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2005}]
}
,

{
trialnum: 57,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pip\u00e9",
option2: "pipi",
option3: "pib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 58,
block: 1,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanf",
option1: "duchanf",
option2: "dochanf",
option3: "dichanf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 59,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beumi",
option2: "beun\u00e9",
option3: "beum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 60,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "dominb",
option3: "diminb",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 61,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gup\u00e9",
option2: "gupi",
option3: "guz\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 62,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "dufeum",
option2: "dofeum",
option3: "difeum",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2037}]
}
,

{
trialnum: 63,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dubip",
option2: "dibip",
option3: "dobip",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2025}]
}
,

{
trialnum: 64,
block: 1,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kapi",
option2: "kab\u00e9",
option3: "kap\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2022}]
}
,

{
trialnum: 65,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "dogeub",
option2: "digeub",
option3: "dugeub",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2040}]
}
,

{
trialnum: 66,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "suf\u00e9",
option2: "suv\u00e9",
option3: "sufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2007}]
}
,

{
trialnum: 67,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "disouv",
option2: "dusouv",
option3: "dosouv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2012}]
}
,

{
trialnum: 68,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouv\u00e9",
option2: "vouvi",
option3: "vouf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2015}]
}
,

{
trialnum: 69,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonp\u00e9",
option2: "jonbi",
option3: "jonb\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2005}]
}
,

{
trialnum: 70,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minbi",
option2: "minp\u00e9",
option3: "minb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 71,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jinch",
option1: "jinj\u00e9",
option2: "jinch\u00e9",
option3: "jinji",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2023}]
}
,

{
trialnum: 72,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "dogonch",
option2: "digonch",
option3: "dugonch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 73,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manb\u00e9",
option2: "manp\u00e9",
option3: "manbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 74,
block: 2,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "ditinch",
option2: "dutinch",
option3: "dotinch",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2024}]
}
,

{
trialnum: 75,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koup\u00e9",
option2: "koubi",
option3: "koub\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2019}]
}
,

{
trialnum: 76,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "dovam",
option2: "divam",
option3: "duvam",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2026}]
}
,

{
trialnum: 77,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bibi",
option2: "bip\u00e9",
option3: "bib\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2025}]
}
,

{
trialnum: 78,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanfi",
option2: "chanv\u00e9",
option3: "chanf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2001}]
}
,

{
trialnum: 79,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "digup",
option2: "dogup",
option3: "dugup",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 80,
block: 2,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dufinch",
option2: "dofinch",
option3: "difinch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 81,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dobeun",
option2: "dubeun",
option3: "dibeun",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 82,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "dogam",
option2: "dugam",
option3: "digam",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2036}]
}
,

{
trialnum: 83,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "divam",
option2: "dovam",
option3: "duvam",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2026}]
}
,

{
trialnum: 84,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "voun\u00e9",
option2: "voum\u00e9",
option3: "vouni",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2021}]
}
,

{
trialnum: 85,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dipib",
option2: "dupib",
option3: "dopib",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2034}]
}
,

{
trialnum: 86,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feun\u00e9",
option2: "feumi",
option3: "feum\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 87,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kun",
option1: "kuni",
option2: "kun\u00e9",
option3: "kum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2013}]
}
,

{
trialnum: 88,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "didoum",
option2: "dodoum",
option3: "dudoum",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 89,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuji",
option2: "kuj\u00e9",
option3: "kuch\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 90,
block: 2,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dibi",
option3: "dib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 91,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fip\u00e9",
option2: "fibi",
option3: "fib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2035}]
}
,

{
trialnum: 92,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "dusouv",
option2: "disouv",
option3: "dosouv",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2012}]
}
,

{
trialnum: 93,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minbi",
option2: "minb\u00e9",
option3: "minp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2029}]
}
,

{
trialnum: 94,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fim\u00e9",
option2: "fin\u00e9",
option3: "fimi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 95,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "duguv",
option2: "doguv",
option3: "diguv",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2006}]
}
,

{
trialnum: 96,
block: 2,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dutinch",
option2: "ditinch",
option3: "dotinch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2024}]
}
,

{
trialnum: 97,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "boup",
option1: "doboup",
option2: "diboup",
option3: "duboup",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 98,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanv\u00e9",
option2: "chanfi",
option3: "chanf\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2001}]
}
,

{
trialnum: 99,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fim\u00e9",
option2: "fimi",
option3: "fin\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2031}]
}
,

{
trialnum: 100,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "doguv",
option2: "duguv",
option3: "diguv",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2006}]
}
,

{
trialnum: 101,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "goufi",
option2: "gouv\u00e9",
option3: "gouf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 102,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "digonb",
option2: "dugonb",
option3: "dogonb",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2008}]
}
,

{
trialnum: 103,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bib\u00e9",
option2: "bibi",
option3: "bip\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2025}]
}
,

{
trialnum: 104,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koub\u00e9",
option2: "koubi",
option3: "koup\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2019}]
}
,

{
trialnum: 105,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dichanv",
option2: "duchanv",
option3: "dochanv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2014}]
}
,

{
trialnum: 106,
block: 2,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "siv",
option1: "siv\u00e9",
option2: "sivi",
option3: "siv\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2033}]
}
,

{
trialnum: 107,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuj\u00e9",
option2: "kuji",
option3: "kuch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2011}]
}
,

{
trialnum: 108,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonp\u00e9",
option2: "jonb\u00e9",
option3: "jonbi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2005}]
}
,

{
trialnum: 109,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feun\u00e9",
option2: "feum\u00e9",
option3: "feumi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2037}]
}
,

{
trialnum: 110,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "dugam",
option2: "dogam",
option3: "digam",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2036}]
}
,

{
trialnum: 111,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "dinif",
option2: "dunif",
option3: "donif",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2030}]
}
,

{
trialnum: 112,
block: 2,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "digeub",
option2: "dogeub",
option3: "dugeub",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2040}]
}
,

{
trialnum: 113,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kap",
option1: "dukap",
option2: "dokap",
option3: "dikap",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 114,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fib\u00e9",
option2: "fibi",
option3: "fip\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2035}]
}
,

{
trialnum: 115,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "monch",
option1: "monj\u00e9",
option2: "monji",
option3: "monch\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2017}]
}
,

{
trialnum: 116,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dofinch",
option2: "difinch",
option3: "dufinch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2038}]
}
,

{
trialnum: 117,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chinp",
option1: "chinbi",
option2: "chinb\u00e9",
option3: "chinp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 118,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dipanp",
option2: "dopanp",
option3: "dupanp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 119,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dukam",
option2: "dikam",
option3: "dokam",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2032}]
}
,

{
trialnum: 120,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "digup",
option2: "dogup",
option3: "dugup",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 121,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koun",
option1: "dokoun",
option2: "dikoun",
option3: "dukoun",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2020}]
}
,

{
trialnum: 122,
block: 2,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "donif",
option2: "dinif",
option3: "dunif",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2030}]
}
,

{
trialnum: 123,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "digonch",
option2: "dogonch",
option3: "dugonch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 124,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouv\u00e9",
option2: "vouf\u00e9",
option3: "vouvi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2015}]
}
,

{
trialnum: 125,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "gouf\u00e9",
option2: "gouv\u00e9",
option3: "goufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 126,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manb\u00e9",
option2: "manp\u00e9",
option3: "manbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 127,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dochanv",
option2: "dichanv",
option3: "duchanv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 128,
block: 2,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dipib",
option2: "dopib",
option3: "dupib",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2034}]
}
,

{
trialnum: 129,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jinch",
option1: "dijinch",
option2: "dojinch",
option3: "dujinch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2023}]
}
,

{
trialnum: 130,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gam\u00e9",
option2: "gani",
option3: "gan\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2036}]
}
,

{
trialnum: 131,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "dofeum",
option2: "difeum",
option3: "dufeum",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2037}]
}
,

{
trialnum: 132,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fip",
option1: "dofip",
option2: "difip",
option3: "dufip",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2035}]
}
,

{
trialnum: 133,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "finch",
option1: "finch\u00e9",
option2: "finj\u00e9",
option3: "finchi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2038}]
}
,

{
trialnum: 134,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "dominb",
option2: "duminb",
option3: "diminb",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 135,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dusuf",
option2: "dosuf",
option3: "disuf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2007}]
}
,

{
trialnum: 136,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doun\u00e9",
option2: "doum\u00e9",
option3: "douni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 137,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "dusiv",
option2: "disiv",
option3: "dosiv",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2033}]
}
,

{
trialnum: 138,
block: 3,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "nif\u00e9",
option2: "niv\u00e9",
option3: "nivi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2030}]
}
,

{
trialnum: 139,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "dogouf",
option2: "digouf",
option3: "dugouf",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 140,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "dumanp",
option2: "domanp",
option3: "dimanp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2009}]
}
,

{
trialnum: 141,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gupi",
option2: "guz\u00e9",
option3: "gup\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2018}]
}
,

{
trialnum: 142,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boup\u00e9",
option2: "boub\u00e9",
option3: "boupi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 143,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dujonp",
option2: "dojonp",
option3: "dijonp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 144,
block: 3,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanf\u00e9",
option2: "chanv\u00e9",
option3: "chanfi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 145,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "fim",
option1: "dofim",
option2: "difim",
option3: "dufim",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 146,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koun",
option1: "koun\u00e9",
option2: "koum\u00e9",
option3: "koumi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2020}]
}
,

{
trialnum: 147,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "boup",
option1: "boup\u00e9",
option2: "boupi",
option3: "boub\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2002}]
}
,

{
trialnum: 148,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonchi",
option2: "gonch\u00e9",
option3: "gonj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 149,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonb",
option1: "gonpi",
option2: "gonb\u00e9",
option3: "gonp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2008}]
}
,

{
trialnum: 150,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vani",
option2: "van\u00e9",
option3: "vam\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2026}]
}
,

{
trialnum: 151,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "panp",
option1: "panp\u00e9",
option2: "panb\u00e9",
option3: "panpi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 152,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "manp",
option1: "dumanp",
option2: "dimanp",
option3: "domanp",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2009}]
}
,

{
trialnum: 153,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "duvouv",
option2: "divouv",
option3: "dovouv",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2015}]
}
,

{
trialnum: 154,
block: 3,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanf",
option1: "duchanf",
option2: "dochanf",
option3: "dichanf",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 155,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gam",
option1: "gam\u00e9",
option2: "gan\u00e9",
option3: "gani",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2036}]
}
,

{
trialnum: 156,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guf\u00e9",
option2: "guv\u00e9",
option3: "gufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 157,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dokun",
option2: "dikun",
option3: "dukun",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 158,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "nif",
option1: "nivi",
option2: "nif\u00e9",
option3: "niv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2030}]
}
,

{
trialnum: 159,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dokuch",
option2: "dikuch",
option3: "dukuch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 160,
block: 3,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "duchinp",
option2: "dichinp",
option3: "dochinp",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 161,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "pib",
option1: "pipi",
option2: "pip\u00e9",
option3: "pib\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2034}]
}
,

{
trialnum: 162,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kuch",
option1: "dikuch",
option2: "dokuch",
option3: "dukuch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2011}]
}
,

{
trialnum: 163,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bip",
option1: "dubip",
option2: "dobip",
option3: "dibip",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2025}]
}
,

{
trialnum: 164,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kun",
option1: "dokun",
option2: "dukun",
option3: "dikun",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2013}]
}
,

{
trialnum: 165,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "monch",
option1: "dumonch",
option2: "domonch",
option3: "dimonch",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2017}]
}
,

{
trialnum: 166,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gouf",
option1: "digouf",
option2: "dogouf",
option3: "dugouf",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2003}]
}
,

{
trialnum: 167,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "geub",
option1: "geup\u00e9",
option2: "geupi",
option3: "geub\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2040}]
}
,

{
trialnum: 168,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kam\u00e9",
option2: "kani",
option3: "kan\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2032}]
}
,

{
trialnum: 169,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gup",
option1: "gup\u00e9",
option2: "gupi",
option3: "guz\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2018}]
}
,

{
trialnum: 170,
block: 3,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tinch",
option1: "tinch\u00e9",
option2: "tinchi",
option3: "tinj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2024}]
}
,

{
trialnum: 171,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "doum",
option1: "doun\u00e9",
option2: "doum\u00e9",
option3: "douni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2004}]
}
,

{
trialnum: 172,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vam",
option1: "vam\u00e9",
option2: "vani",
option3: "van\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2026}]
}
,

{
trialnum: 173,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beun\u00e9",
option2: "beum\u00e9",
option3: "beumi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2028}]
}
,

{
trialnum: 174,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "suf",
option1: "dosuf",
option2: "disuf",
option3: "dusuf",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2007}]
}
,

{
trialnum: 175,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "siv",
option1: "disiv",
option2: "dusiv",
option3: "dosiv",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2033}]
}
,

{
trialnum: 176,
block: 3,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vouv",
option1: "dovouv",
option2: "duvouv",
option3: "divouv",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2015}]
}
,

{
trialnum: 177,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "voun",
option1: "duvoun",
option2: "divoun",
option3: "dovoun",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2021}]
}
,

{
trialnum: 178,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koub",
option1: "dokoub",
option2: "dikoub",
option3: "dukoub",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2019}]
}
,

{
trialnum: 179,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanv",
option1: "chanv\u00e9",
option2: "chanfi",
option3: "chanf\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2014}]
}
,

{
trialnum: 180,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "guv",
option1: "guf\u00e9",
option2: "guv\u00e9",
option3: "gufi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2006}]
}
,

{
trialnum: 181,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jonp",
option1: "dojonp",
option2: "dujonp",
option3: "dijonp",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 182,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "souv",
option1: "souv\u00e9",
option2: "souf\u00e9",
option3: "soufi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 183,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "dib",
option1: "dodib",
option2: "didib",
option3: "dudib",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2027}]
}
,

{
trialnum: 184,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "beun",
option1: "beumi",
option2: "beun\u00e9",
option3: "beum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2028}]
}
,

{
trialnum: 185,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kap",
option1: "kapi",
option2: "kap\u00e9",
option3: "kab\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 186,
block: 3,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chinp",
option1: "duchinp",
option2: "dochinp",
option3: "dichinp",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2039}]
}
,

{
trialnum: 187,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonb",
option1: "gonp\u00e9",
option2: "gonb\u00e9",
option3: "gonpi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2008}]
}
,

{
trialnum: 188,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "minb",
option1: "duminb",
option2: "dominb",
option3: "diminb",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 189,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kam",
option1: "kan\u00e9",
option2: "kam\u00e9",
option3: "kani",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2032}]
}
,

{
trialnum: 190,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "dib",
option1: "dudib",
option2: "didib",
option3: "dodib",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2027}]
}
,

{
trialnum: 191,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "feum",
option1: "dufeum",
option2: "dofeum",
option3: "difeum",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2037}]
}
,

{
trialnum: 192,
block: 3,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gonch",
option1: "gonch\u00e9",
option2: "gonchi",
option3: "gonj\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 193,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonb",
option1: "dogonb",
option2: "digonb",
option3: "dugonb",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2008}]
}
,

{
trialnum: 194,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dochanv",
option2: "dichanv",
option3: "duchanv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 195,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "goufi",
option2: "gouv\u00e9",
option3: "gouf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2003}]
}
,

{
trialnum: 196,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "vouni",
option2: "voun\u00e9",
option3: "voum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2021}]
}
,

{
trialnum: 197,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dibi",
option3: "dib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 198,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dukam",
option2: "dikam",
option3: "dokam",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2032}]
}
,

{
trialnum: 199,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dufinch",
option2: "dofinch",
option3: "difinch",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2038}]
}
,

{
trialnum: 200,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "beun",
option1: "dobeun",
option2: "dibeun",
option3: "dubeun",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2028}]
}
,

{
trialnum: 201,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "guv",
option1: "duguv",
option2: "diguv",
option3: "doguv",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2006}]
}
,

{
trialnum: 202,
block: 4,
miniblock: 1,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manbi",
option2: "manp\u00e9",
option3: "manb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2009}]
}
,

{
trialnum: 203,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonb\u00e9",
option2: "jonp\u00e9",
option3: "jonbi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2005}]
}
,

{
trialnum: 204,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bibi",
option2: "bip\u00e9",
option3: "bib\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2025}]
}
,

{
trialnum: 205,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "digup",
option2: "dogup",
option3: "dugup",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2018}]
}
,

{
trialnum: 206,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kun",
option1: "kum\u00e9",
option2: "kun\u00e9",
option3: "kuni",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2013}]
}
,

{
trialnum: 207,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "divam",
option2: "duvam",
option3: "dovam",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2026}]
}
,

{
trialnum: 208,
block: 4,
miniblock: 1,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "digonch",
option2: "dugonch",
option3: "dogonch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2010}]
}
,

{
trialnum: 209,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "ditinch",
option2: "dotinch",
option3: "dutinch",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2024}]
}
,

{
trialnum: 210,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanf\u00e9",
option2: "chanfi",
option3: "chanv\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 211,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koup\u00e9",
option2: "koubi",
option3: "koub\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2019}]
}
,

{
trialnum: 212,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "feum",
option1: "feumi",
option2: "feun\u00e9",
option3: "feum\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2037}]
}
,

{
trialnum: 213,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kap",
option1: "dokap",
option2: "dukap",
option3: "dikap",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2022}]
}
,

{
trialnum: 214,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gup",
option1: "dogup",
option2: "dugup",
option3: "digup",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2018}]
}
,

{
trialnum: 215,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kun",
option1: "kuni",
option2: "kun\u00e9",
option3: "kum\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2013}]
}
,

{
trialnum: 216,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kuch",
option1: "kuji",
option2: "kuch\u00e9",
option3: "kuj\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2011}]
}
,

{
trialnum: 217,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "dosouv",
option2: "disouv",
option3: "dusouv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2012}]
}
,

{
trialnum: 218,
block: 4,
miniblock: 2,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "siv",
option1: "sivi",
option2: "siv\u00e9",
option3: "siv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2033}]
}
,

{
trialnum: 219,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fib\u00e9",
option2: "fip\u00e9",
option3: "fibi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2035}]
}
,

{
trialnum: 220,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "boup",
option1: "doboup",
option2: "diboup",
option3: "duboup",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2002}]
}
,

{
trialnum: 221,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minb\u00e9",
option2: "minbi",
option3: "minp\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2029}]
}
,

{
trialnum: 222,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "chanv",
option1: "dochanv",
option2: "dichanv",
option3: "duchanv",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2014}]
}
,

{
trialnum: 223,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "dudoum",
option2: "dodoum",
option3: "didoum",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2004}]
}
,

{
trialnum: 224,
block: 4,
miniblock: 2,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fin\u00e9",
option2: "fimi",
option3: "fim\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2031}]
}
,

{
trialnum: 225,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fip",
option1: "fip\u00e9",
option2: "fibi",
option3: "fib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2035}]
}
,

{
trialnum: 226,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "bip",
option1: "bib\u00e9",
option2: "bibi",
option3: "bip\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2025}]
}
,

{
trialnum: 227,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "boup",
option1: "duboup",
option2: "diboup",
option3: "doboup",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2002}]
}
,

{
trialnum: 228,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chinp",
option1: "chinb\u00e9",
option2: "chinbi",
option3: "chinp\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2039}]
}
,

{
trialnum: 229,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jonp",
option1: "jonbi",
option2: "jonb\u00e9",
option3: "jonp\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2005}]
}
,

{
trialnum: 230,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "jinch",
option1: "jinj\u00e9",
option2: "jinch\u00e9",
option3: "jinji",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2023}]
}
,

{
trialnum: 231,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "geub",
option1: "digeub",
option2: "dogeub",
option3: "dugeub",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2040}]
}
,

{
trialnum: 232,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "vam",
option1: "divam",
option2: "dovam",
option3: "duvam",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2026}]
}
,

{
trialnum: 233,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "monch",
option1: "monji",
option2: "monj\u00e9",
option3: "monch\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2017}]
}
,

{
trialnum: 234,
block: 4,
miniblock: 3,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gonch",
option1: "digonch",
option2: "dogonch",
option3: "dugonch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2010}]
}
,

{
trialnum: 235,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "chanf",
option1: "chanfi",
option2: "chanf\u00e9",
option3: "chanv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2001}]
}
,

{
trialnum: 236,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dopanp",
option2: "dupanp",
option3: "dipanp",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2016}]
}
,

{
trialnum: 237,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "gouf",
option1: "goufi",
option2: "gouf\u00e9",
option3: "gouv\u00e9",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2003}]
}
,

{
trialnum: 238,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "voun",
option1: "voun\u00e9",
option2: "vouni",
option3: "voum\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2021}]
}
,

{
trialnum: 239,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "digam",
option2: "dugam",
option3: "dogam",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2036}]
}
,

{
trialnum: 240,
block: 4,
miniblock: 3,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "kam",
option1: "dokam",
option2: "dukam",
option3: "dikam",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2032}]
}
,

{
trialnum: 241,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "suf",
option1: "sufi",
option2: "suv\u00e9",
option3: "suf\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2007}]
}
,

{
trialnum: 242,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gam",
option1: "digam",
option2: "dogam",
option3: "dugam",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2036}]
}
,

{
trialnum: 243,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "pib",
option1: "dopib",
option2: "dipib",
option3: "dupib",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2034}]
}
,

{
trialnum: 244,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "panp",
option1: "dipanp",
option2: "dopanp",
option3: "dupanp",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2016}]
}
,

{
trialnum: 245,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "doum",
option1: "dodoum",
option2: "dudoum",
option3: "didoum",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2004}]
}
,

{
trialnum: 246,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "vouv",
option1: "vouvi",
option2: "vouf\u00e9",
option3: "vouv\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2015}]
}
,

{
trialnum: 247,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "nif",
option1: "dinif",
option2: "dunif",
option3: "donif",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2030}]
}
,

{
trialnum: 248,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "minb",
option1: "minbi",
option2: "minp\u00e9",
option3: "minb\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2029}]
}
,

{
trialnum: 249,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "koun",
option1: "dukoun",
option2: "dokoun",
option3: "dikoun",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2020}]
}
,

{
trialnum: 250,
block: 4,
miniblock: 4,
blockType: "training",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fim",
option1: "fin\u00e9",
option2: "fim\u00e9",
option3: "fimi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2031}]
}
,

{
trialnum: 251,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "tinch",
option1: "dotinch",
option2: "dutinch",
option3: "ditinch",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2024}]
}
,

{
trialnum: 252,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "dib",
option1: "dib\u00e9",
option2: "dibi",
option3: "dib\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2027}]
}
,

{
trialnum: 253,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "koub",
option1: "koub\u00e9",
option2: "koup\u00e9",
option3: "koubi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2019}]
}
,

{
trialnum: 254,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "manp",
option1: "manp\u00e9",
option2: "manb\u00e9",
option3: "manbi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2009}]
}
,

{
trialnum: 255,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "finch",
option1: "dofinch",
option2: "difinch",
option3: "dufinch",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2038}]
}
,

{
trialnum: 256,
block: 4,
miniblock: 4,
blockType: "test",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "souv",
option1: "disouv",
option2: "dusouv",
option3: "dosouv",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2012}]
}
,

{
trialnum: 257,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jaut",
option1: "dijaut",
option2: "dojaut",
option3: "dujaut",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2042}]
}
,

{
trialnum: 258,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "baud",
option1: "baut\u00e9",
option2: "baud\u00e9",
option3: "baudi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2041}]
}
,

{
trialnum: 259,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tait",
option1: "tait\u00e9",
option2: "taidi",
option3: "taid\u00e9",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2047}]
}
,

{
trialnum: 260,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "mait",
option1: "dumait",
option2: "dimait",
option3: "domait",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2048}]
}
,

{
trialnum: 261,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaud",
option1: "kaud\u00e9",
option2: "kaudi",
option3: "kaut\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2045}]
}
,

{
trialnum: 262,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bap",
option1: "dibap",
option2: "dubap",
option3: "dobap",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2050}]
}
,

{
trialnum: 263,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fiv",
option1: "fivi",
option2: "fif\u00e9",
option3: "fiv\u00e9",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2049}]
}
,

{
trialnum: 264,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "daid",
option1: "dodaid",
option2: "dudaid",
option3: "didaid",
option1type: "foil",
option2type: "distractor",
option3type: "target",
image: 2044}]
}
,

{
trialnum: 265,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaid",
option1: "kaid\u00e9",
option2: "kait\u00e9",
option3: "kaidi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2043}]
}
,

{
trialnum: 266,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gaut",
option1: "dugaut",
option2: "dogaut",
option3: "digaut",
option1type: "distractor",
option2type: "target",
option3type: "foil",
image: 2046}]
}
,

{
trialnum: 267,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "fiv",
option1: "fif\u00e9",
option2: "fiv\u00e9",
option3: "fivi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2049}]
}
,

{
trialnum: 268,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "jaut",
option1: "dijaut",
option2: "dojaut",
option3: "dujaut",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2042}]
}
,

{
trialnum: 269,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaud",
option1: "kaud\u00e9",
option2: "kaudi",
option3: "kaut\u00e9",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2045}]
}
,

{
trialnum: 270,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "mait",
option1: "dimait",
option2: "domait",
option3: "dumait",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2048}]
}
,

{
trialnum: 271,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "daid",
option1: "dudaid",
option2: "dodaid",
option3: "didaid",
option1type: "distractor",
option2type: "foil",
option3type: "target",
image: 2044}]
}
,

{
trialnum: 272,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "baud",
option1: "baut\u00e9",
option2: "baud\u00e9",
option3: "baudi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2041}]
}
,

{
trialnum: 273,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "tait",
option1: "taid\u00e9",
option2: "tait\u00e9",
option3: "taidi",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2047}]
}
,

{
trialnum: 274,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "bap",
option1: "dibap",
option2: "dubap",
option3: "dobap",
option1type: "target",
option2type: "distractor",
option3type: "foil",
image: 2050}]
}
,

{
trialnum: 275,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "size",
learningType: "supervised",
rootWord: "gaut",
option1: "dogaut",
option2: "digaut",
option3: "dugaut",
option1type: "target",
option2type: "foil",
option3type: "distractor",
image: 2046}]
}
,

{
trialnum: 276,
block: 5,
miniblock: 5,
blockType: "generalization",
contents:  [{
grammarType: "plural",
learningType: "reinforcement",
rootWord: "kaid",
option1: "kait\u00e9",
option2: "kaid\u00e9",
option3: "kaidi",
option1type: "foil",
option2type: "target",
option3type: "distractor",
image: 2043}]
}
,
];export default learningStudy